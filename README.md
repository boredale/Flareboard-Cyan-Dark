# Flareboard-Cyan-Dark

This dark theme applies to [Flareboard](https://flareboard.ru) imageboard.

## Preview

![Desktop posts](images/posts.png "Desktop main page")

![Mobile posts](images/posts-mobile.png "Mobile main page")

## Installation

First you need to install userstyle extension like [Stylus](https://github.com/openstyles/stylus/)

Then install [usercss](https://codeberg.org/boredale/Flareboard-Cyan-Dark/raw/branch/main/flareboard-cyan-dark.user.css) using Stylus manager

Alternatively you can install [userstyle](https://userstyles.world/style/2758/flareboard-cyan-dark) from UserStyles.world. If you get a "Style not found" error, just refresh the page.

## Contributions

[Pull requests](https://codeberg.org/boredale/Flareboard-Cyan-Dark/pulls) are welcome.
